package com.mmarins.calendar.repository;

import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.mmarins.calendar.config.TestConfig;
import com.mmarins.calendar.domain.Event;

/**
 *
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
public class EventRepositoryTest {

    private static final int ZERO = 0;
    private static final Logger LOGGER = LoggerFactory.getLogger(EventRepositoryTest.class);

    @Inject
    private EventRepository<Event> eventRepository;

    @Test
    public void testInject() {
        assertThat("The repository should be injected", eventRepository, notNullValue());
    }

    @Test
    public void testRead() throws Exception {
        try {
            final Collection<Event> events = eventRepository.findAll();
            assertThat("events.size() must not be bigger than zero", events.size(), greaterThan(ZERO));
        } catch (final Exception e) {
            LOGGER.error(e.getMessage(), e);
            fail();
        }
    }

    @Test
    public void testWrite() throws Exception {
        try {
            final Collection<Event> events = eventRepository.findAll();
            final int initialSize = events.size();
            Collection<Event> newEvents = createJsonSample();
            eventRepository.saveAll(newEvents);
            newEvents = eventRepository.findAll();
            assertEquals("the expected collection size should be " + initialSize + 2, events.size() + 2, newEvents.size());

        } catch (final Exception e) {
            LOGGER.error(e.getMessage());
            fail();
        }
    }

    public List<Event> createJsonSample() throws ParseException {
        final List<Event> events = new ArrayList<Event>();
        events.add(new Event("Montly Miami Geek Meeting", Calendar.getInstance().getTime(), Calendar.getInstance().getTime(), null));
        events.add(new Event("Study Algorithms", Calendar.getInstance().getTime(), Calendar.getInstance().getTime(), null));
        return events;
    }

}