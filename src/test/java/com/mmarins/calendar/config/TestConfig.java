package com.mmarins.calendar.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.mmarins.calendar")
public class TestConfig {

}
