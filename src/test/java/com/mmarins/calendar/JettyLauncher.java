package com.mmarins.calendar;

import java.io.File;
import java.io.FileInputStream;

import org.eclipse.jetty.annotations.AnnotationConfiguration;
import org.eclipse.jetty.plus.webapp.EnvConfiguration;
import org.eclipse.jetty.plus.webapp.PlusConfiguration;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.FragmentConfiguration;
import org.eclipse.jetty.webapp.MetaInfConfiguration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.webapp.WebInfConfiguration;
import org.eclipse.jetty.webapp.WebXmlConfiguration;
import org.eclipse.jetty.xml.XmlConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This Class is responsible to start the Jetty's embedded container.
 */
public class JettyLauncher extends Thread {

    private static final Logger LOG = LoggerFactory.getLogger(JettyLauncher.class);

    @Override
    public void start() {
        try {
            LOG.info("Starting Jetty Web Server...");
            final File baseDir = new File(".");

            final FileInputStream in = new FileInputStream(new File(baseDir, "src/test/resources/jetty.xml"));
            final XmlConfiguration configuration = new XmlConfiguration(in);
            final Server server = (Server) configuration.configure();

            final File webappDir = new File(baseDir, "src/main/webapp/");
            final WebAppContext context = new WebAppContext();
            context.setContextPath("/calendar");
            context.setResourceBase(webappDir.getAbsolutePath());

            // Configuration classes. This gives support for multiple features.
            // The annotationConfiguration is required to support annotations like @WebServlet
            context.setConfigurations(new Configuration[]{ //
                    new AnnotationConfiguration(), new WebXmlConfiguration(), new WebInfConfiguration(), //
                    new PlusConfiguration(), new MetaInfConfiguration(), new FragmentConfiguration(), new EnvConfiguration() //
            });

            // Important! make sure Jetty scans all classes under ./classes looking for annotations.
            // Classes directory is generated running 'mvn package'
            context.setAttribute("org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern", ".*/classes/.*");
            context.setParentLoaderPriority(true);

            // Config and launch server
            server.setHandler(context);
            server.start();

            server.start();
            LOG.info("Jetty Web Server on the fly...");

            server.join();
            LOG.info("Jetty Web Server stopped!");

        } catch (final Exception e) {
            e.printStackTrace();
            System.exit(100);
        }
    }

    /**
     * Run this test to start the application embedded.
     *
     * @param args
     */
    public static void main(final String[] args) {
        final JettyLauncher webServer = new JettyLauncher();
        webServer.start();
    }

}