package com.mmarins.calendar.entity;

import static com.mmarins.calendar.Configs.ISO_8601_DATE_TIME_UTC;
import static com.mmarins.calendar.Configs.UTC;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Entity to handle a specific type of Date.
 * <p>
 * Date and time expressed according to ISO 8601
 * ISO_8601_DATE_TIME_UTC = yyyy-MM-dd'T'HH:mm'Z'
 */
public class Moment {

    private Calendar calendar;

    /**
     * Constructor.
     *
     * @param date
     */
    public Moment(final Date date) {
        calendar = Calendar.getInstance();
        calendar.setTime(date);
    }

    /**
     * Constructor.
     *
     * @param calendar
     *            @link Calendar
     */
    public Moment(final Calendar calendar) {
        this.calendar = calendar;
    }

    /**
     * Constructor.
     *
     * @param strDate
     * @throws ParseException
     */
    public Moment(final String strDate) throws ParseException {
        if ((strDate != null) && !strDate.equals("")) {
            final SimpleDateFormat sdf = new SimpleDateFormat(ISO_8601_DATE_TIME_UTC);
            calendar = Calendar.getInstance();
            calendar.setTime(sdf.parse(strDate));

        } else {
            final String message = MessageFormat.format("Error: strDate cannot be null or empty [{0}]", strDate);
            throw new ParseException(message, 1);
        }
    }

    private static DateFormat getMomentFormat() {
        final TimeZone tz = TimeZone.getTimeZone(UTC);
        final DateFormat sdf = new SimpleDateFormat(ISO_8601_DATE_TIME_UTC);
        sdf.setTimeZone(tz);
        return sdf;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public Date getDate() {
        return calendar.getTime();
    }

    public String getString() {
        final DateFormat df = getMomentFormat();
        return df.format(getDate());
    }

}