package com.mmarins.calendar.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mmarins.calendar.exceptions.FileException;

/**
 * Utility Object to handle files.
 */
public class FileUtil {

    private static Logger logger = LoggerFactory.getLogger(FileUtil.class);

    /**
     * @param resourceFileName
     * @return String with content
     * @throws FileException
     */
    public static String readFromResourceInClassLoader(final String resourceFileName) throws FileException {
        try {
            String strContent = null;
            final ClassLoader cl = Thread.currentThread().getContextClassLoader();
            final InputStream inputStream = cl.getResourceAsStream(resourceFileName);
            if (inputStream != null) {
                strContent = IOUtils.toString(inputStream);
            } else {
                throw new FileException("Resource \"" + resourceFileName + "\" not found in the ClassLoader");
            }
            IOUtils.closeQuietly(inputStream);
            return strContent;

        } catch (final IOException e) {
            logger.error(e.getMessage(), e);
            throw new FileException(e.getMessage(), e);
        }
    }

    /**
     * @param file
     * @return String with content
     * @throws FileException
     */
    public static String readFromFile(final File file) throws FileException {
        try {
            final InputStream inputStream = new FileInputStream(file);
            final String strContent = IOUtils.toString(inputStream);
            IOUtils.closeQuietly(inputStream);
            return strContent;

        } catch (final Exception e) {
            logger.error(e.getMessage(), e);
            throw new FileException(e.getMessage(), e);
        }
    }

    /**
     * @param file
     * @param data
     * @throws FileException
     */
    public static void writeToFile(final File file, final String data) throws FileException {
        try {
            final OutputStream out = new FileOutputStream(file);
            IOUtils.write(data, out);
            IOUtils.closeQuietly(out);

        } catch (final Exception e) {
            logger.error(e.getMessage(), e);
            throw new FileException(e.getMessage(), e);
        }
    }

}