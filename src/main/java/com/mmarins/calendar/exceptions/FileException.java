package com.mmarins.calendar.exceptions;

/**
 * Object to handle Exceptions in file's I/O operations.
 *
 * @see Exception
 */
public class FileException extends Exception {

    private static final long serialVersionUID = -1447136764425042339L;

    public FileException() {
        super();
    }

    public FileException(final String message) {
        super(message);
    }

    public FileException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FileException(final Throwable cause) {
        super(cause);
    }

}