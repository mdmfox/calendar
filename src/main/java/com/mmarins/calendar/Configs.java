package com.mmarins.calendar;

/**
 * Class only to keep 'Constants Configs' of the Calendar application.
 */
public class Configs {

    // 'FullCalendar' Date Configuration
    public static final String UTC = "UTC";
    public static final String ISO_8601_DATE_TIME_UTC = "yyyy-MM-dd'T'HH:mm";

    // Data Base Configurations
    public static final String INITIAL_DB = "META-INF/initial-db.json";
    public static final String DB_DIR = "./target/database";
    public static final String DB_FILE = "calendar-db.json";

}