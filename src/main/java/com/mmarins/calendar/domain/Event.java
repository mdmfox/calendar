package com.mmarins.calendar.domain;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.mmarins.calendar.entity.Moment;

/**
 * This class represents Event Entity Object.
 * <p>
 * Used to handle JSON of the view 'Date.jsp'
 */
public class Event implements Serializable {

    private static final long serialVersionUID = 8222428129325660604L;

    /**
     * Uniquely identifies the given event.
     */
    private int id;

    /**
     * The text on an event's element.
     */
    private String title;

    /**
     * The date/time an event begins. Required.
     * A Moment-ish input, like an ISO8601 string. Throughout the API this will
     * become a real Moment object.
     */
    private String start;

    /**
     * The exclusive date/time an event ends. <b>Optional</b>.
     * A Moment-ish input, like an ISO8601 string. Throughout the API this will
     * become a real Moment object.
     * It is the moment immediately after the event has ended. For example, if
     * the last full day of an event is Thursday, the exclusive end of the event
     * will be 00:00:00 on Friday!
     */
    private String end;

    /**
     * String. <b>Optional</b>. A URL that will be visited when this event is clicked
     * by the user. For more information on controlling this behavior, see the
     * eventClick callback.
     */
    private String url;

    /**
     * A CSS class (or array of classes) that will be attached to this event's
     * element.
     */
    private String className;

    /**
     * Whether an event occurs at a specific time-of-day. This property affects
     * whether an event's time is shown. Also, in the agenda views, determines
     * if it is displayed in the "all-day" section.
     * Default value is 'False'
     */
    private boolean allDay = false;

    /**
     * Overrides the master editable option for this single event.
     * Default value is 'True'
     */
    private boolean editable = true;

    /**
     * Overrides the master eventStartEditable option for this single event.
     * Default value is 'True'
     */
    private boolean startEditable = true;

    /**
     * Overrides the master eventDurationEditable option for this single event.
     * Default value is 'True'
     */
    private boolean durationEditable = true;

    /**
     * Allows alternate rendering of the event, like background events.
     * Can be empty, "background", or "inverse-background"
     */
    private String rendering;

    /**
     * Overrides the master eventOverlap option for this single event.
     * If false, prevents this event from being dragged/resized over other
     * events. Also prevents other events from being dragged/resized over this
     * event.
     * Default value is 'False'
     */
    private boolean overlap = false;

    /**
     * Sets an event's background and border color just like the Date-wide
     * 'Color' option.
     */
    private String color;

    /**
     * Sets an event's background color just like the Date-wide 'Color'
     * option.
     */
    private String backgroundColor;

    /**
     * Sets an event's border color just like the the Date-wide 'Color'
     * option.
     */
    private String borderColor;

    /**
     * Sets an event's text color just like the Date-wide 'Color' option.
     */
    private String textColor;

    /**
     * Default constructor because of serialization.
     */
    public Event() {
    }

    /**
     * Constructor.
     *
     * @param title
     * @param start
     * @param end
     * @param url
     */
    public Event(final String title, final Date start, final Date end, final String url) {
        super();
        this.title = title;
        this.url = url;
        this.start = new Moment(start).getString();

        this.end = (end == null) ? "" : new Moment(end).getString();
    }

    /**
     * Constructor.
     *
     * @param title
     * @param start
     * @param end
     * @param url
     * @throws ParseException
     */
    public Event(final String title, final String start, final String end, final String url) throws ParseException {
        super();
        this.title = title;
        this.url = url;
        this.start = new Moment(start).getString();

        this.end = (StringUtils.isBlank(end)) ? "" : new Moment(end).getString();
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getStart() {
        return start;
    }

    public void setStart(final String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(final String end) {
        this.end = end;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(final String className) {
        this.className = className;
    }

    public boolean isAllDay() {
        return allDay;
    }

    public void setAllDay(final boolean allDay) {
        this.allDay = allDay;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(final boolean editable) {
        this.editable = editable;
    }

    public boolean isStartEditable() {
        return startEditable;
    }

    public void setStartEditable(final boolean startEditable) {
        this.startEditable = startEditable;
    }

    public boolean isDurationEditable() {
        return durationEditable;
    }

    public void setDurationEditable(final boolean durationEditable) {
        this.durationEditable = durationEditable;
    }

    public String getRendering() {
        return rendering;
    }

    public void setRendering(final String rendering) {
        this.rendering = rendering;
    }

    public boolean isOverlap() {
        return overlap;
    }

    public void setOverlap(final boolean overlap) {
        this.overlap = overlap;
    }

    public String getColor() {
        return color;
    }

    public void setColor(final String color) {
        this.color = color;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(final String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(final String borderColor) {
        this.borderColor = borderColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(final String textColor) {
        this.textColor = textColor;
    }

}