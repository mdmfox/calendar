package com.mmarins.calendar.service;

import java.util.Collection;

import com.mmarins.calendar.domain.Event;

/**
 * Service that sign the Services of Calendar.
 */
public interface CalendarService extends Service {

    /**
     * @return Collection<Event>
     */
    Collection<Event> getEvents();

    /**
     * @param events
     *            Collection<Event>
     */
    void saveEvents(Collection<Event> events);

    /**
     * @param event
     *            Event
     */
    void saveEvent(Event event);

}