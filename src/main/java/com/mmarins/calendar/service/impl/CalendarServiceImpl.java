package com.mmarins.calendar.service.impl;

import java.util.Collection;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.mmarins.calendar.domain.Event;
import com.mmarins.calendar.repository.EventRepository;
import com.mmarins.calendar.service.CalendarService;

/**
 * Service that sign the Services of Calendar.
 * <p>
 * This is responsible to handle all Controller operations
 * of Calendar application.
 */
@Service("calendarService")
public class CalendarServiceImpl implements CalendarService {

    @Inject
    private EventRepository<Event> eventRepository;

    @Override
    public Collection<Event> getEvents() {
        return eventRepository.findAll();
    }

    @Override
    public void saveEvent(final Event event) {
        eventRepository.save(event);
    }

    @Override
    public void saveEvents(final Collection<Event> events) {
        eventRepository.saveAll(events);
    }

}