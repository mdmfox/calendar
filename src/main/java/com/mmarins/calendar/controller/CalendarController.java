package com.mmarins.calendar.controller;

import static com.mmarins.calendar.Configs.ISO_8601_DATE_TIME_UTC;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.GsonBuilder;
import com.mmarins.calendar.domain.Event;
import com.mmarins.calendar.service.CalendarService;

/**
 * Control the Web Services of the Main page.
 * <p>
 * Responsible to all 'Calendar' actions
 */
@Controller
@RequestMapping("/event")
public class CalendarController {

    private final Logger logger = LoggerFactory.getLogger(CalendarController.class);

    private final GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat(ISO_8601_DATE_TIME_UTC);

    // private static final String ERROR_TAG = "error";

    @Inject
    CalendarService calendarService;

    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public ModelAndView main() { //
        final ModelAndView main = new ModelAndView("main");
        return main;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public Collection<Event> list(final Model model) {
        Collection<Event> events = new ArrayList<Event>();
        try {
            events = calendarService.getEvents();

        } catch (final Throwable e) {
            // model.addAttribute(ERROR_TAG, "Generic error:" + e.getMessage());
            logger.error(e.getMessage(), e);
        }
        return events;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public Event save(@RequestBody final String strEvent, final Model model) {
        Event event = new Event();
        try {
            event = gsonBuilder.create().fromJson(strEvent, Event.class);
            calendarService.saveEvent(event);

        } catch (final Throwable e) {
            // model.addAttribute(ERROR_TAG, "Generic error:" + e.getMessage());
            logger.error(e.getMessage(), e);
        }
        return event;
    }

    //    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    //    @ResponseBody
    //    public boolean delete(@RequestBody final String strEvent) {
    //        Event event = gsonBuilder.create().fromJson(strEvent, Event.class);
    //        calendarService.delete(event);
    //    }

}