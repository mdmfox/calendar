package com.mmarins.calendar.repository;

import java.util.Collection;

/**
 * Define the base actions of EventRepository.
 *
 * @param <Event>
 */
public interface EventRepository<Event> extends Repository<Event> {

    @Override
    Collection<Event> findAll();

    @Override
    void saveAll(Collection<Event> events);

    @Override
    Event findById(int id);

}