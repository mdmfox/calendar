package com.mmarins.calendar.repository.impl;

import static com.mmarins.calendar.Configs.DB_DIR;
import static com.mmarins.calendar.Configs.DB_FILE;
import static com.mmarins.calendar.Configs.INITIAL_DB;
import static com.mmarins.calendar.Configs.ISO_8601_DATE_TIME_UTC;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mmarins.calendar.domain.Event;
import com.mmarins.calendar.exceptions.FileException;
import com.mmarins.calendar.repository.EventRepository;
import com.mmarins.calendar.utils.FileUtil;

/**
 * The implementation of EventRepository.
 * <p>
 * This is responsible to handle all Persistence operations
 * of Event objects.
 */
@Repository("eventRepository")
public class EventRepositoryImpl implements EventRepository<Event> {

    private final Logger logger = LoggerFactory.getLogger(EventRepositoryImpl.class);

    private int lastId = 1;
    private final Map<Integer, Event> eventsMap = new TreeMap<>();

    private final File dir = new File(DB_DIR);
    private final File dbf = new File(dir, DB_FILE);

    private final GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat(ISO_8601_DATE_TIME_UTC);
    private final Type eventType = new TypeToken<Collection<Event>>() {
    }.getType();

    /**
     * Constructor
     * <p>
     * There is a unique instance (as singleton) of this class which is responsible to manage the
     * 'Data Base'. To keep it simple, was made the choice to keep it in file.
     * All data is stored in json format inside of this 'Data Base'.
     */
    public EventRepositoryImpl() {
        // check if is necessary create a database file ...
        final boolean createDB = !dir.exists() || !dbf.exists();
        dir.mkdirs();
        if (createDB) {
            initializeDatabase();
        } else {
            loadDatabase();
        }
    }

    /**
     * At the very first time or after any 'mvn clean' command, it reads the 'sample' in the
     * INITIAL_DB.
     * The path is inside of the application, it means that it was packaged with the .war
     */
    private void initializeDatabase() {
        try {
            final String data = FileUtil.readFromResourceInClassLoader(INITIAL_DB);
            loadEventsFromString(data);

        } catch (final FileException e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * After the application have initialized the 'Data Base', the system will manage its own 'Data
     * Base' file.
     * All operations by the users make in runtime past to be stored in this path in the 'target
     * path'.
     * Always when you clean the application server. The system will have to initialize the 'Data
     * Base' from the sample.
     * <p>
     * This have been chosen because this application is just an presentation sample.
     */
    private void loadDatabase() {
        try {
            final String data = FileUtil.readFromFile(dbf);
            loadEventsFromString(data);

        } catch (final FileException e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * @param data
     */
    private void loadEventsFromString(final String data) {
        final List<Event> events = gsonBuilder.create().fromJson(data, eventType);
        for (final Event event : events) {
            checkEventId(event);
            eventsMap.put(event.getId(), event);
        }
    }

    /**
     * This method is responsible for the integrity of the 'Data Base'.
     *
     * @param event
     */
    private synchronized void checkEventId(final Event event) {
        if (event.getId() != 0) {
            if (event.getId() > lastId) {
                lastId = event.getId() + 1;
            }
        } else {
            event.setId(++lastId);
        }
    }

    @Override
    public Collection<Event> findAll() {
        final Gson gson = gsonBuilder.create();
        final String data = gson.toJson(eventsMap.values());
        final Collection<Event> result = gson.fromJson(data, eventType);
        return result;
    }

    @Override
    public Event findById(final int id) {
        Event result = eventsMap.get(id);
        if (result != null) {
            final Gson gson = gsonBuilder.create();
            final String data = gson.toJson(eventsMap.values());
            result = gson.fromJson(data, Event.class);
        }
        return result;
    }

    @Override
    public void save(final Event event) {
        final Collection<Event> events = new ArrayList<>();
        events.add(event);
        saveAll(events);
    }

    @Override
    public void saveAll(final Collection<Event> events) {
        try {
            for (final Event event : events) {
                checkEventId(event);
                eventsMap.put(event.getId(), event);
            }
            final String data = gsonBuilder.create().toJson(eventsMap.values());
            FileUtil.writeToFile(dbf, data);

        } catch (final FileException e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

}