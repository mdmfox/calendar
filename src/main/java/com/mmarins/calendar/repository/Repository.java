package com.mmarins.calendar.repository;

import java.util.Collection;

/**
 * Define the base CRUD actions of one Repository.
 * <p>
 * Type Parameters: <T>
 * T - the component type of the array
 *
 * @param <T>
 */
public interface Repository<T> {

    /**
     * Find an Domain Object by id.
     *
     * @param id
     * @return domain object
     */
    T findById(int id);

    /**
     * Find all Domain Objects.
     *
     * @return Collection of Repository Object
     */
    Collection<T> findAll();

    /**
     * Save or Update a list of Domain Object.
     *
     * @param collection
     *            of Repository Object
     */
    void saveAll(Collection<T> collection);

    /**
     * Save or Update a Domain Object.
     *
     * @param Repository
     *            Object
     */
    void save(T object);

}