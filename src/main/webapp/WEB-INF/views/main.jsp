<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8' />
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link href='../resources/libs/fullcalendar-2.6.1/fullcalendar.min.css' rel='stylesheet' />
<link href='../resources/libs/fullcalendar-2.6.1/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='../resources/libs/fullcalendar-2.6.1/lib/moment.min.js'></script>
<script src='../resources/libs/fullcalendar-2.6.1/lib/jquery.min.js'></script>
<script src='../resources/libs/fullcalendar-2.6.1/fullcalendar.min.js'></script>
<script src="../resources/js/app/main.js"></script>

<style>

	body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}

</style>
</head>
<body>

	<div id='calendar'></div>

</body>
</html>
