$(document).ready(function() {
		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			selectable: true,
			selectHelper: true,
			eventResize: function(event) { 
				save(event); 
			},
			eventDrop: function(event) { 
				save(event);
			},		
			select: function(start, end) {
				var title = prompt('Create your event:');			
				if (title) {
					var eventData = { title: title, start: start, end: end };
					save(eventData, function(data) {
						$('#calendar').fullCalendar('renderEvent', data, true);
						$('#calendar').fullCalendar('unselect');
					});
				}
				$('#calendar').fullCalendar('unselect');
			},
			editable : true,
			eventLimit : true, // allow "more" link when too many events
			events : {
				url : 'list'
			}
		});

		function save(event, callback) {
			var eventData = {
				id: event.id,
				title: event.title,
				start: moment(event.start).format("YYYY-MM-DDTHH:mm"),
				end: moment(event.end).format("YYYY-MM-DDTHH:mm")
			};
			$.ajax({
				type : 'POST',
				url : 'save',
				data : JSON.stringify(eventData),
				contentType : 'application/json',
				//dataType : "json",
				success : function(data) {
					if (callback != undefined) {
						callback(data);
					}
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
				}
			});
		}

});